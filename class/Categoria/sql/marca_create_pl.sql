-- Function: public.marca_register(integer, character varying, integer)

-- DROP FUNCTION public.marca_register(integer, character varying, integer);

CREATE OR REPLACE FUNCTION public.marca_register(i_id integer, i_nombre character varying, i_productor_id integer)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.marca
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO public.marca(
                                nombre,
                                productor_id)
                        VALUES (
                                i_nombre,
                                i_productor_id);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE public.marca SET
                        productor_id=i_productor_id
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.marca_register(integer, character varying, integer)
  OWNER TO postgres;

-- Function: public.marca_remove(integer)

-- DROP FUNCTION public.marca_remove(integer);

CREATE OR REPLACE FUNCTION public.marca_remove(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.marca WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.marca_remove(integer)
  OWNER TO postgres;

