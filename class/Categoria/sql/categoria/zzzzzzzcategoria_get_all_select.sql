SELECT 
     A.id, A.descripcion ||' ('||B.descripcion||')' AS ente, A.id_aux, B.descripcion AS ente_tipo, A.id_ente_tipo, A.alias
FROM admini.ente A
    INNER JOIN admini.ente_tipo B ON A.id_ente_tipo = B.id
{fld:where}
ORDER BY 1,2;
