<?php
namespace myApp1\Producto;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** Presentacion: Clase para actualizar y consultar la tabla 'presentacion'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Presentacion
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase Presentacion.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase Presentacion.*/
    const PATH = __DIR__ . '/sql/presentacion/';

    /** Actualiza registro de la tabla 'presentacion'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'presentacion'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $_POST['id_usuario'] = $_SESSION['id_usuario'];
        //$_POST['empaque'] = json_encode($_POST['empaque']);
        //Func::seeArray($_POST);
        //$_POST['estatus'] = array_key_exists("estatus",$_POST) ? 't' : 'f';        
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'producto'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'presentacion'.
    * Descripción: Obtener registro(s) de la tabla 'presentacion'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'presentacion'.*/
    public function get()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::PATH . "get.sql", $_POST, false, '', 'producto'));
    }

    /** Elimina registro de la tabla 'presentacion'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'presentacion'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO', 'producto'));
        return $row[0];
    }

}

