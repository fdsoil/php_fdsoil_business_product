<?php
namespace myApp1;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** Producto: Clase para actualizar y consultar la tabla 'producto'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Producto
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase Producto.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase Producto.*/
    const PATH = __DIR__ . '/sql/producto/';

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Producto a traves de la
    * variable $_POST, para actualizar la tabla 'producto'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "nombre" => [
            "label" => "Nombre",
            "required" => false
        ],
        "id_marca" => [
            "label" => "Id Marca",
            "required" => false
        ],
        "id_categoria" => [
            "label" => "Id Categoria",
            "required" => false
        ]
    ];

    /** Obtener registro(s) de la tabla 'producto'.
    * Descripción: Obtener registro(s) de la tabla 'producto'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'producto'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'producto'.*/
    public function get($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'CATEGORY_GROUP':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id_categoria = {fld:id} ');
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id = {fld:id} ');
                break;
        }
        return DbFunc::exeQryFile(self::PATH . "get.sql", $arr, false, '', 'producto');
    }

    /** Actualiza registro de la tabla 'producto'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'producto'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST['id_usuario'] = $_SESSION['id_usuario'];
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'producto'));
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
        }
        return $msj;
    }

    /** Elimina registro de la tabla 'producto'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'producto'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO', 'producto'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    /** Lista de tabla foránea 'categoria'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'categoria'.
    * @return result Resultado de la consulta de la de tabla foránea 'categoria'.*/ 
    public function categoriaList()
    {
        return DbFunc::exeQryFile(self::PATH . 'categoria_list_select.sql', $_POST, false, '', 'producto');
    }

    /** Lista de tabla foránea 'marca'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'marca'.
    * @return result Resultado de la consulta de la de tabla foránea 'marca'.*/ 
    public function marcaList()
    {
        return DbFunc::exeQryFile(self::PATH . 'marca_list_select.sql', $_POST, false, '', 'producto');
    }

}

