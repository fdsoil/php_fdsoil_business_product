SELECT 
    A.id,
    A.nombre,
    A.id_marca,
    A.id_categoria
    ,B.nombre AS des_categoria
    ,B.familia AS des_categoria_familia
    ,C.nombre AS des_marca
FROM public.producto A
INNER JOIN public.view_categoria B ON A.id_categoria=B.id
INNER JOIN public.marca C ON A.id_marca=C.id
{fld:where}
ORDER BY 2;
