SELECT 
    id,
    id_producto,
    medida_unidad,
    empaque as empaque_json,
    (select presentacion_despliegue(id)) as empaque_desplegado,
    precio_costo,
    precio_venta,
    bar_cod,
    int_cod--,estatus    
FROM public.presentacion 
WHERE id_producto = {fld:id}
ORDER BY 2;
