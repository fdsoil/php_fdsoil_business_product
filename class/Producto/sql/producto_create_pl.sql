-- Function: public.producto_register(integer, character varying, integer, integer)

-- DROP FUNCTION public.producto_register(integer, character varying, integer, integer);

CREATE OR REPLACE FUNCTION public.producto_register(i_id integer, i_nombre character varying, i_id_marca integer, i_id_categoria integer)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.producto
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO public.producto(
                                nombre,
                                id_marca,
                                id_categoria)
                        VALUES (
                                i_nombre,
                                i_id_marca,
                                i_id_categoria);
                        SELECT max(id) INTO v_id FROM public.producto
                                WHERE nombre=i_nombre;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE public.producto SET
                        id_marca=i_id_marca,
                        id_categoria=i_id_categoria
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.producto_register(integer, character varying, integer, integer)
  OWNER TO postgres;

-- Function: public.producto_remove(integer)

-- DROP FUNCTION public.producto_remove(integer);

CREATE OR REPLACE FUNCTION public.producto_remove(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.producto WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.producto_remove(integer)
  OWNER TO postgres;

-- Function: public.presentacion_register(integer, character varying, integer, character varying, jsonb, double precision, double precision, character varying, character varying, boolean, integer, integer, integer, integer, date, date, time without time zone, time without time zone)

-- DROP FUNCTION public.presentacion_register(integer, character varying, integer, character varying, jsonb, double precision, double precision, character varying, character varying, boolean, integer, integer, integer, integer, date, date, time without time zone, time without time zone);

CREATE OR REPLACE FUNCTION public.presentacion_register(i_id integer, i_nombre character varying, i_id_producto integer, i_medida_unidad character varying, i_empaque jsonb, i_precio_costo double precision, i_precio_venta double precision, i_bar_cod character varying, i_int_cod character varying, i_estatus boolean, i_id_user_insert integer, i_id_user_update integer, i_id_user_edit integer, i_editing integer, i_date_insert date, i_date_update date, i_time_insert time without time zone, i_time_update time without time zone)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO public.presentacion(
                        nombre,
                        id_producto,
                        medida_unidad,
                        empaque,
                        precio_costo,
                        precio_venta,
                        bar_cod,
                        int_cod,
                        estatus,
                        id_user_insert,
                        id_user_update,
                        id_user_edit,
                        editing,
                        date_insert,
                        date_update,
                        time_insert,
                        time_update)
                VALUES (
                        i_nombre,
                        i_id_producto,
                        i_medida_unidad,
                        i_empaque,
                        i_precio_costo,
                        i_precio_venta,
                        i_bar_cod,
                        i_int_cod,
                        i_estatus,
                        i_id_user_insert,
                        i_id_user_update,
                        i_id_user_edit,
                        i_editing,
                        i_date_insert,
                        i_date_update,
                        i_time_insert,
                        i_time_update);
                o_return:= 'C';
        ELSE
                UPDATE public.presentacion SET
                        nombre=i_nombre,
                        id_producto=i_id_producto,
                        medida_unidad=i_medida_unidad,
                        empaque=i_empaque,
                        precio_costo=i_precio_costo,
                        precio_venta=i_precio_venta,
                        bar_cod=i_bar_cod,
                        int_cod=i_int_cod,
                        estatus=i_estatus,
                        id_user_insert=i_id_user_insert,
                        id_user_update=i_id_user_update,
                        id_user_edit=i_id_user_edit,
                        editing=i_editing,
                        date_insert=i_date_insert,
                        date_update=i_date_update,
                        time_insert=i_time_insert,
                        time_update=i_time_update
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.presentacion_register(integer, character varying, integer, character varying, jsonb, double precision, double precision, character varying, character varying, boolean, integer, integer, integer, integer, date, date, time without time zone, time without time zone)
  OWNER TO postgres;

-- Function: public.presentacion_remove(integer)

-- DROP FUNCTION public.presentacion_remove(integer);

CREATE OR REPLACE FUNCTION public.presentacion_remove(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.presentacion WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.presentacion_remove(integer)
  OWNER TO postgres;

