--drop USER MAPPING FOR postgres SERVER localhost_negocio_seguridad;
--drop foreign table seguridad_usuario;
--drop SERVER localhost_negocio_seguridad;

CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'negocio_seguridad', port '5432');

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_seguridad OPTIONS(user 'postgres', password 'postgres');

CREATE FOREIGN TABLE seguridad_usuario (id integer, usuario character varying, correo character varying,
cedula character varying, nombre character varying, apellido character varying ) 
SERVER localhost_negocio_seguridad OPTIONS(schema_name 'seguridad', table_name 'usuario');

SELECT id, usuario, correo, cedula, nombre, apellido
  FROM seguridad_usuario;



/*




CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '162.252.57.24', dbname 'ejjc_negocio_seguridad', port '5544');

CREATE USER MAPPING FOR admin SERVER localhost_negocio_seguridad OPTIONS(user 'admin', password 'admin');

CREATE FOREIGN TABLE seguridad_usuario (id integer, usuario character varying, correo character varying,
cedula character varying, nombre character varying, apellido character varying ) 
SERVER localhost_negocio_seguridad OPTIONS(schema_name 'seguridad', table_name 'usuario');

SELECT id, usuario, correo, cedula, nombre, apellido
  FROM seguridad_usuario;

  --drop USER MAPPING FOR admin SERVER localhost_negocio_seguridad;
--drop foreign table seguridad_usuario;
--drop SERVER localhost_negocio_seguridad;*/




--Gregorio
--drop USER MAPPING FOR postgres SERVER localhost_negocio_seguridad;
--drop foreign table seguridad_usuario;
--drop SERVER localhost_negocio_seguridad;

CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '169.10.1.3', dbname 'ejjc_negocio_seguridad', port '5432');

CREATE USER MAPPING FOR admin SERVER localhost_negocio_seguridad OPTIONS(user 'admin', password 'admin');

CREATE FOREIGN TABLE seguridad_usuario (id integer, usuario character varying, correo character varying,
cedula character varying, nombre character varying, apellido character varying ) 
SERVER localhost_negocio_seguridad OPTIONS(schema_name 'seguridad', table_name 'usuario');

SELECT id, usuario, correo, cedula, nombre, apellido
  FROM seguridad_usuario;

