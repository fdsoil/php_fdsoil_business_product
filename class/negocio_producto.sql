--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

-- Started on 2020-07-11 22:24:14 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 3 (class 3079 OID 179117)
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- TOC entry 2 (class 3079 OID 188651)
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- TOC entry 254 (class 1255 OID 179685)
-- Name: categoria_register(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.categoria_register(i_id integer, i_padre_id integer, i_nombre character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.categoria
                        WHERE nombre=i_nombre AND padre_id = i_padre_id;
                IF  v_existe='f' THEN
                        INSERT INTO public.categoria(
                                nombre,
                                padre_id)
                        VALUES (
                                i_nombre,
                                i_padre_id);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE public.categoria SET
                        nombre = i_nombre,
                        padre_id=i_padre_id
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.categoria_register(i_id integer, i_padre_id integer, i_nombre character varying) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 179686)
-- Name: categoria_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.categoria_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.categoria WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.categoria_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 188752)
-- Name: marca_register(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.marca_register(i_id integer, i_nombre character varying, i_productor_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.marca
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO public.marca(
                                nombre,
                                productor_id)
                        VALUES (
                                i_nombre,
                                i_productor_id);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE public.marca SET
                        productor_id=i_productor_id
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.marca_register(i_id integer, i_nombre character varying, i_productor_id integer) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 179681)
-- Name: marca_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.marca_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.marca WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.marca_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 179762)
-- Name: presentacion_despliegue(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.presentacion_despliegue(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character varying;
        v_arr json[];
        v_uni_med character varying;
        v_arr_union character varying[];
        v_n integer;
BEGIN
         o_return := '';
         SELECT (ARRAY(select json_array_elements(empaque::json))), medida_unidad  INTO v_arr, v_uni_med FROM presentacion WHERE id=i_id;
         --https://stackoverflow.com/questions/9783422/loop-over-array-dimension-in-plpgsql
         --https://kb.objectrocket.com/postgresql/postgres-loop-types-1027
         v_arr_union = ARRAY[' DE ', ' CON ']; 
         v_n = 1;        
         FOR i IN array_lower(v_arr, 1) .. array_upper(v_arr, 1) LOOP                  
                  IF  i = 1 THEN
                      o_return := v_arr[i]->'packing' || v_arr_union[v_n] || (v_arr[i]->'quantity'::text) || ' ' || v_uni_med;
                  ELSE
                      o_return := (v_arr[i]->'packing') || v_arr_union[v_n] || (v_arr[i]->'quantity') || ' ' || o_return;
                  END IF;
                  v_n = v_n + 1;
                  IF v_n = 3 THEN
                      v_n = 1;
                  END IF;
         END LOOP;
         
        RETURN (replace(o_return, '"', ''));
END;
$$;


ALTER FUNCTION public.presentacion_despliegue(i_id integer) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 189359)
-- Name: presentacion_register(integer, integer, character varying, jsonb, double precision, double precision, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.presentacion_register(i_id integer, i_id_producto integer, i_medida_unidad character varying, i_empaque jsonb, i_precio_costo double precision, i_precio_venta double precision, i_bar_cod character varying, i_int_cod character varying, i_id_user integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO public.presentacion(
                        id_producto,
                        medida_unidad,
                        empaque,
                        precio_costo,
                        precio_venta,
                        bar_cod,
                        int_cod,
                        id_user_insert,
                        id_user_update)
                VALUES (
                        i_id_producto,
                        i_medida_unidad,
                        i_empaque,
                        i_precio_costo,
                        i_precio_venta,
                        i_bar_cod,
                        i_int_cod,
                        i_id_user,
                        i_id_user);
                o_return:= 'C';
        ELSE
                UPDATE public.presentacion SET
                        id_producto=i_id_producto,
                        medida_unidad=i_medida_unidad,
                        empaque=i_empaque,
                        precio_costo=i_precio_costo,
                        precio_venta=i_precio_venta,
                        bar_cod=i_bar_cod,
                        int_cod=i_int_cod,
                        id_user_update=i_id_user,
                        date_update=('now'::text)::date,
                        time_update=('now'::text)::time without time zone
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.presentacion_register(i_id integer, i_id_producto integer, i_medida_unidad character varying, i_empaque jsonb, i_precio_costo double precision, i_precio_venta double precision, i_bar_cod character varying, i_int_cod character varying, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 179701)
-- Name: presentacion_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.presentacion_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.presentacion WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.presentacion_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 179748)
-- Name: producto_register(integer, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.producto_register(i_id integer, i_nombre character varying, i_id_marca integer, i_id_categoria integer, i_id_user integer) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.producto
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO public.producto(
                                nombre,
                                id_marca,
                                id_categoria)
                        VALUES (
                                i_nombre,
                                i_id_marca,
                                i_id_categoria);
                        SELECT max(id) INTO v_id FROM public.producto
                                WHERE nombre=i_nombre;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE public.producto SET
                        id_marca=i_id_marca,
                        id_categoria=i_id_categoria,
                        id_user_update=i_id_user,
                        id_user_edit=i_id_user,
                        editing=true,
                        date_update=('now'::text)::date,
                        time_update=('now'::text)::time without time zone
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.producto_register(i_id integer, i_nombre character varying, i_id_marca integer, i_id_categoria integer, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 179699)
-- Name: producto_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.producto_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.producto WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.producto_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 1748 (class 1417 OID 188655)
-- Name: localhost_negocio_seguridad; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'negocio_seguridad',
    host 'localhost',
    port '5432'
);


ALTER SERVER localhost_negocio_seguridad OWNER TO postgres;

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 1748
-- Name: USER MAPPING postgres SERVER localhost_negocio_seguridad; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_seguridad OPTIONS (
    password 'postgres',
    "user" 'postgres'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 189 (class 1259 OID 179608)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    padre_id integer DEFAULT 0 NOT NULL,
    final boolean DEFAULT true NOT NULL
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 179606)
-- Name: catagoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catagoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catagoria_id_seq OWNER TO postgres;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 188
-- Name: catagoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catagoria_id_seq OWNED BY public.categoria.id;


--
-- TOC entry 195 (class 1259 OID 179657)
-- Name: presentacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.presentacion (
    id integer NOT NULL,
    id_producto integer NOT NULL,
    medida_unidad character varying(30) NOT NULL,
    empaque jsonb NOT NULL,
    precio_costo double precision NOT NULL,
    precio_venta double precision NOT NULL,
    bar_cod character varying(15) NOT NULL,
    int_cod character varying(15) NOT NULL,
    estatus boolean DEFAULT true NOT NULL,
    id_user_insert integer NOT NULL,
    id_user_update integer NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.presentacion OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 179655)
-- Name: id_presentacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.id_presentacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_presentacion_seq OWNER TO postgres;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 194
-- Name: id_presentacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.id_presentacion_seq OWNED BY public.presentacion.id;


--
-- TOC entry 193 (class 1259 OID 179636)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.producto (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    id_marca integer NOT NULL,
    id_categoria integer NOT NULL,
    id_user_insert integer DEFAULT 1 NOT NULL,
    id_user_update integer DEFAULT 1 NOT NULL,
    id_user_edit integer DEFAULT 1 NOT NULL,
    editing boolean DEFAULT true NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 179634)
-- Name: id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_producto_seq OWNER TO postgres;

--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 192
-- Name: id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.id_producto_seq OWNED BY public.producto.id;


--
-- TOC entry 191 (class 1259 OID 179617)
-- Name: marca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.marca (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    productor_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.marca OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 179615)
-- Name: marcas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.marcas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marcas_id_seq OWNER TO postgres;

--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 190
-- Name: marcas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.marcas_id_seq OWNED BY public.marca.id;


--
-- TOC entry 197 (class 1259 OID 188657)
-- Name: seguridad_usuario; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.seguridad_usuario (
    id integer,
    usuario character varying,
    correo character varying,
    cedula character varying,
    nombre character varying,
    apellido character varying
)
SERVER localhost_negocio_seguridad
OPTIONS (
    schema_name 'seguridad',
    table_name 'usuario'
);


ALTER FOREIGN TABLE public.seguridad_usuario OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 179751)
-- Name: view_categoria; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_categoria AS
 WITH RECURSIVE categoria_recursivo(id, nombre, familia, padre_id, nivel, final) AS (
         SELECT raiz.id,
            raiz.nombre,
            ((''::character varying)::text || (raiz.nombre)::text) AS familia,
            raiz.padre_id,
            1 AS nivel,
            raiz.final
           FROM public.categoria raiz
          WHERE (raiz.padre_id = 0)
        UNION ALL
         SELECT hijo.id,
            hijo.nombre,
            concat(concat(padre.familia, ' / '), hijo.nombre) AS concat,
            hijo.padre_id,
            (padre.nivel + 1) AS nivel,
            hijo.final
           FROM categoria_recursivo padre,
            public.categoria hijo
          WHERE (padre.id = hijo.padre_id)
        )
 SELECT categoria_recursivo.id,
    categoria_recursivo.nombre,
    categoria_recursivo.familia,
    categoria_recursivo.padre_id,
    categoria_recursivo.nivel,
    categoria_recursivo.final
   FROM categoria_recursivo;


ALTER TABLE public.view_categoria OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 189348)
-- Name: view_presentacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_presentacion AS
 SELECT a.id,
    c.familia AS categoria,
    b.nombre AS producto,
    d.nombre AS marca,
    a.empaque AS empaque_json,
    a.medida_unidad,
    a.precio_costo,
    a.precio_venta,
    a.bar_cod,
    a.int_cod
   FROM (((public.presentacion a
     JOIN public.producto b ON ((a.id_producto = b.id)))
     JOIN public.view_categoria c ON ((b.id_categoria = c.id)))
     JOIN public.marca d ON ((b.id_marca = d.id)))
  ORDER BY c.familia, b.nombre, d.nombre;


ALTER TABLE public.view_presentacion OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 188696)
-- Name: view_producto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_producto AS
 SELECT a.id,
    b.familia AS categoria,
    a.nombre AS producto,
    c.nombre AS marca
   FROM ((public.producto a
     JOIN public.view_categoria b ON ((a.id_categoria = b.id)))
     JOIN public.marca c ON ((a.id_marca = c.id)))
  ORDER BY a.id, b.familia;


ALTER TABLE public.view_producto OWNER TO postgres;

--
-- TOC entry 2101 (class 2604 OID 179611)
-- Name: categoria id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria ALTER COLUMN id SET DEFAULT nextval('public.catagoria_id_seq'::regclass);


--
-- TOC entry 2104 (class 2604 OID 179620)
-- Name: marca id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca ALTER COLUMN id SET DEFAULT nextval('public.marcas_id_seq'::regclass);


--
-- TOC entry 2115 (class 2604 OID 179660)
-- Name: presentacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion ALTER COLUMN id SET DEFAULT nextval('public.id_presentacion_seq'::regclass);


--
-- TOC entry 2106 (class 2604 OID 179639)
-- Name: producto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto ALTER COLUMN id SET DEFAULT nextval('public.id_producto_seq'::regclass);


--
-- TOC entry 2122 (class 2606 OID 179614)
-- Name: categoria catagoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT catagoria_pkey PRIMARY KEY (id);


--
-- TOC entry 2124 (class 2606 OID 179677)
-- Name: marca marca_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca
    ADD CONSTRAINT marca_nombre_key UNIQUE (nombre);


--
-- TOC entry 2126 (class 2606 OID 179633)
-- Name: marca marca_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca
    ADD CONSTRAINT marca_pkey PRIMARY KEY (id);


--
-- TOC entry 2132 (class 2606 OID 189355)
-- Name: presentacion presentacion_id_producto_medida_unidad_empaque_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_id_producto_medida_unidad_empaque_key UNIQUE (id_producto, medida_unidad, empaque);


--
-- TOC entry 2134 (class 2606 OID 189357)
-- Name: presentacion presentacion_int_cod_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_int_cod_key UNIQUE (int_cod);


--
-- TOC entry 2136 (class 2606 OID 179670)
-- Name: presentacion presentacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2128 (class 2606 OID 179693)
-- Name: producto producto_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_nombre_key UNIQUE (nombre);


--
-- TOC entry 2130 (class 2606 OID 179641)
-- Name: producto producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- TOC entry 2139 (class 2606 OID 214478)
-- Name: presentacion presentacion_id_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_id_producto_fkey FOREIGN KEY (id_producto) REFERENCES public.producto(id);


--
-- TOC entry 2137 (class 2606 OID 179642)
-- Name: producto producto_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_categoria_id_foreign FOREIGN KEY (id_categoria) REFERENCES public.categoria(id);


--
-- TOC entry 2138 (class 2606 OID 179687)
-- Name: producto producto_marca_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_marca_id_fkey FOREIGN KEY (id_marca) REFERENCES public.marca(id);


-- Completed on 2020-07-11 22:24:15 -04

--
-- PostgreSQL database dump complete
--

