CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'negocio_seguridad', port '5432');

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_seguridad OPTIONS(user 'postgres', password 'postgres');

CREATE FOREIGN TABLE seguridad_usuario (id integer, usuario character varying, correo character varying,
cedula character varying, nombre character varying, apellido character varying ) 
SERVER localhost_negocio_seguridad OPTIONS(schema_name 'seguridad', table_name 'usuario');

SELECT id, usuario, correo, cedula, nombre, apellido
  FROM seguridad_usuario;






////////////////////////////////////



SELECT 
    A.id,
    A.nombre,
    B.nombre as producto,
    C.familia as categoria,

    A.medida_unidad,
    A.empaque as empaque_json,
    (select presentacion_despliegue(A.id)) as empaque_desplegado,
    A.precio_costo,
    A.precio_venta,
    A.bar_cod,
    A.int_cod--,estatus    
FROM public.presentacion A
INNER JOIN public.producto B ON A.id_producto = B.id
INNER JOIN public.view_categoria_recursivo C ON B.id_categoria = C.id
ORDER BY 3;

