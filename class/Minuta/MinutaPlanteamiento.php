<?php
namespace myApp1\Minuta;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** MinutaPlanteamiento: Clase para actualizar y consultar la tabla 'minuta_planteamiento'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MinutaPlanteamiento
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase MinutaPlanteamiento.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase MinutaPlanteamiento.*/
    const PATH = __DIR__ . '/sql/minuta_planteamiento/';

    /** Actualiza registro de la tabla 'minuta_planteamiento'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'minuta_planteamiento'.
    * Descripción: Obtener registro(s) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'minuta_planteamiento'.*/
    public function get()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::PATH . "get.sql", $_POST));
    }

    /** Elimina registro de la tabla 'minuta_planteamiento'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

}

