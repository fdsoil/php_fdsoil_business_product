SELECT 
    id,
    id_minuta,
    planteamiento,
    ponente,
    observacion
FROM minuta.minuta_planteamiento 
WHERE id_minuta = {fld:id}
ORDER BY 3;
