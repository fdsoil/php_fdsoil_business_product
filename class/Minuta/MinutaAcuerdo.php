<?php
namespace myApp1\Minuta;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** MinutaAcuerdo: Clase para actualizar y consultar la tabla 'minuta_acuerdo'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MinutaAcuerdo
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase MinutaAcuerdo.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase MinutaAcuerdo.*/
    const PATH = __DIR__ . '/sql/minuta_acuerdo/';

    /** Actualiza registro de la tabla 'minuta_acuerdo'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'minuta_acuerdo'.
    * Descripción: Obtener registro(s) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'minuta_acuerdo'.*/
    public function get()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::PATH . "get.sql", $_POST));
    }

    /** Elimina registro de la tabla 'minuta_acuerdo'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

}

