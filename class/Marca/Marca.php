<?php
namespace myApp1;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** Marca: Clase para actualizar y consultar la tabla 'marca'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Marca
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase Marca.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase Marca.*/
    const PATH = __DIR__ . '/sql/marca/';

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Marca a traves de la
    * variable $_POST, para actualizar la tabla 'marca'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "nombre" => [
            "label" => "Nombre",
            "required" => false
        ],
        "productor_id" => [
            "label" => "Productor Id",
            "required" => false
        ]
    ];

    /** Obtener registro(s) de la tabla 'marca'.
    * Descripción: Obtener registro(s) de la tabla 'marca'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'marca'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'marca'.*/
    public function get($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE id = {fld:id} ');
                break;
        }
        return DbFunc::exeQryFile(self::PATH . "get.sql", $arr, false, '', 'producto');
    }

    /** Actualiza registro de la tabla 'marca'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'marca'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'producto'));
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
            $np = 1;
        }
        Func::adminMsj($msj,$np);
    }

    /** Elimina registro de la tabla 'marca'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'marca'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO', 'producto'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

}

