<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;
use \FDSoil\XTemplate;
use \myApp1\Marca;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Marca();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Marca::get('REGIST'))
                :\FDSoil\DbFunc::iniRegist('marca','public', 'producto');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        //$xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        //$xtpl->assign('PRODUCTOR_ID', $aRegist['productor_id']);
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "marca"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
