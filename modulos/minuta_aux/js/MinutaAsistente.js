var MinutaAsistente = (function () {

    const MYPATH = PATH_MYAPP1 + "reqs/control/minuta/minuta_asistente.php/";

    let initObjsPanel = function() {
        initObjs(document.getElementById('id_panel_minuta_asistente'));
    }

    let rqsRegister = function() {
        let responseAjax = (response) => {
            msjAdmin(response);
            if (inArray(response, ['C', 'A'])){
                reqGet();
                MinutaAsistente.onOffPanel();
            }
        }
        let sIdVal = document.getElementById('id').value;
        let oPanel = document.getElementById('id_panel_minuta_asistente');
        let reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.b64 = true;
        ajax.send();
    }

    let reqGet = function (bAsync) {
        let responseAjax = (response) => {
            fillTab(response);
            valBtnClose();
        }
        let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "get";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'json';
        ajax.async = bAsync;
        ajax.b64 = true;
        ajax.send();
    }

    let fillTab = function (aObjs) {

        let fillTabAux = function(row) {

            let oTr = document.createElement('tr');
            oTr.id = row.id;
            let aTd = [];

            aTd[0] = document.createElement('td');
            aTd[0].appendChild(document.createTextNode(row.cedula));
            oTr.appendChild(aTd[0]);

            aTd[1] = document.createElement('td');
            aTd[1].appendChild(document.createTextNode(row.nombre));
            oTr.appendChild(aTd[1]);

            aTd[2] = document.createElement('td');
            aTd[2].appendChild(document.createTextNode(row.correo));
            oTr.appendChild(aTd[2]);

            aTd[3] = document.createElement('td');
            aTd[3].appendChild(document.createTextNode(row.telefono));
            oTr.appendChild(aTd[3]);

            aTd[4] = document.createElement('td');
            aTd[4].id = row.id_ente
            aTd[4].appendChild(document.createTextNode(row.des_ente));
            oTr.appendChild(aTd[4]);

            aTd[5] = document.createElement('td');
            aTd[5].id = row.id_dependencia
            aTd[5].appendChild(document.createTextNode(row.des_dependencia));
            oTr.appendChild(aTd[5]);

            aTd[6] = document.createElement('td');
            aTd[6].id = row.id_cargo
            aTd[6].appendChild(document.createTextNode(row.des_cargo));
            oTr.appendChild(aTd[6]);

            aTd[7] = document.createElement('td');
            aTd[7].appendChild(document.createTextNode(row.observacion));
            oTr.appendChild(aTd[7]);

            let oImgEdit = document.createElement('img');
            oImgEdit.setAttribute('class', 'accion');
            oImgEdit.setAttribute('src', PATH_APPORG +'img/edit.png');
            oImgEdit.setAttribute('title', 'Editar datos...');
            oImgEdit.setAttribute('onclick',"MinutaAsistente.edit(this);valBtnClose();");
            aTd[8] = document.createElement('td');
            aTd[8].align = 'center';
            aTd[8].appendChild(oImgEdit);

            let oImgDelete = document.createElement('img');
            oImgDelete.setAttribute('class', 'accion');
            oImgDelete.setAttribute('src', PATH_APPORG + 'img/cross.png');
            oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
            oImgDelete.setAttribute('onclick',"MinutaAsistente.rqsRemove("+row.id+");valBtnClose();");
            aTd[8].appendChild(oImgDelete);
            oTr.appendChild(aTd[8]);

            return oTr;

        }

        let tbl = document.getElementById('id_tab_minuta_asistente');
        Table.deleteAllRows('id_tab_minuta_asistente',1);

        if (!tbl.tBodies[0]) {
            var tbody = document.createElement('tbody');
            tbody.setAttribute('style', 'display: block');
            tbody.setAttribute('style', 'width: 100%');
            tbl.appendChild(tbody);
        }

        if (aObjs.length !== 0) {
            for (var i = 0; i < aObjs.length; i++)
                tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
            Table.paintTRsClearDark('id_tab_minuta_asistente');
        }

    }

    return {

        onOffPanel: function () {
            toggle("id_panel_minuta_asistente", "blind",500);
            changeTheObDeniedWritingImg("id_img_minuta_asistente");
            initObjsPanel();
            document.getElementById("id_minuta_asistente").value=0;
        },

        valEnvio: function () {
            if (validateObjs(document.getElementById("id_panel_minuta_asistente")))
                rqsRegister();
        },

        edit: function (obj) {
            if(document.getElementById('id_panel_minuta_asistente').style.display == "none")
                MinutaAsistente.onOffPanel();
            document.getElementById("id_minuta_asistente").value = obj.parentNode.parentNode.id;
            let oPanel = document.getElementById("id_panel_minuta_asistente");
            let oInputs = oPanel.getElementsByTagName("input");
            oInputs['cedula'].value = obj.parentNode.parentNode.cells[0].innerHTML;
            oInputs['nombre'].value = obj.parentNode.parentNode.cells[1].innerHTML;
            oInputs['correo'].value = obj.parentNode.parentNode.cells[2].innerHTML;
            oInputs['telefono'].value = obj.parentNode.parentNode.cells[3].innerHTML;
            let oSelects=oPanel.getElementsByTagName("select");
            oSelects['id_ente'].value = obj.parentNode.parentNode.cells[4].id;
            oSelects['id_dependencia'].value = obj.parentNode.parentNode.cells[5].id;
            oSelects['id_cargo'].value = obj.parentNode.parentNode.cells[6].id;
            let oTextAreas=oPanel.getElementsByTagName("textarea");
            oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[7].innerHTML;
        },

        rqsRemove: function (id) {
            confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
                if (ok) {
                    let responseAjax = (response) => {
                        (response != 'B') ? msjAdmin(response) : reqGet();
                    }
                    let ajax = new sendAjax();
                    ajax.method = 'POST';
                    ajax.url = MYPATH + "remove";
                    ajax.data = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
                    ajax.funResponse = responseAjax;
                    ajax.b64 = true;
                    ajax.send();
                }
            });
        }

    };

})();

