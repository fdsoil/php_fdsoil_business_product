var MinutaPlanteamiento = (function () {

    const MYPATH = PATH_MYAPP1 + "reqs/control/minuta/minuta_planteamiento.php/";

    let initObjsPanel = function() {
        initObjs(document.getElementById('id_panel_minuta_planteamiento'));
    }

    let rqsRegister = function() {
        let responseAjax = (response) => {
            msjAdmin(response);
            if (inArray(response, ['C', 'A'])){
                reqGet();
                MinutaPlanteamiento.onOffPanel();
            }
        }
        let sIdVal = document.getElementById('id').value;
        let oPanel = document.getElementById('id_panel_minuta_planteamiento');
        let reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.b64 = true;
        ajax.send();
    }

    let reqGet = function (bAsync) {
        let responseAjax = (response) => {
            fillTab(response);
            valBtnClose();
        }
        let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "get";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'json';
        ajax.async = bAsync;
        ajax.b64 = true;
        ajax.send();
    }

    let fillTab = function (aObjs) {

        let fillTabAux = function(row) {

            let oTr = document.createElement('tr');
            oTr.id = row.id;
            let aTd = [];

            aTd[0] = document.createElement('td');
            aTd[0].appendChild(document.createTextNode(row.ponente));
            oTr.appendChild(aTd[0]);

            aTd[1] = document.createElement('td');
            aTd[1].appendChild(document.createTextNode(row.planteamiento));
            oTr.appendChild(aTd[1]);

            aTd[2] = document.createElement('td');
            aTd[2].appendChild(document.createTextNode(row.observacion));
            oTr.appendChild(aTd[2]);

            let oImgEdit = document.createElement('img');
            oImgEdit.setAttribute('class', 'accion');
            oImgEdit.setAttribute('src', PATH_APPORG +'img/edit.png');
            oImgEdit.setAttribute('title', 'Editar datos...');
            oImgEdit.setAttribute('onclick',"MinutaPlanteamiento.edit(this);valBtnClose();");
            aTd[3] = document.createElement('td');
            aTd[3].align = 'center';
            aTd[3].appendChild(oImgEdit);

            let oImgDelete = document.createElement('img');
            oImgDelete.setAttribute('class', 'accion');
            oImgDelete.setAttribute('src', PATH_APPORG + 'img/cross.png');
            oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
            oImgDelete.setAttribute('onclick',"MinutaPlanteamiento.rqsRemove("+row.id+");valBtnClose();");
            aTd[3].appendChild(oImgDelete);
            oTr.appendChild(aTd[3]);

            return oTr;

        }

        let tbl = document.getElementById('id_tab_minuta_planteamiento');
        Table.deleteAllRows('id_tab_minuta_planteamiento',1);

        if (!tbl.tBodies[0]) {
            var tbody = document.createElement('tbody');
            tbody.setAttribute('style', 'display: block');
            tbody.setAttribute('style', 'width: 100%');
            tbl.appendChild(tbody);
        }

        if (aObjs.length !== 0) {
            for (var i = 0; i < aObjs.length; i++)
                tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
            Table.paintTRsClearDark('id_tab_minuta_planteamiento');
        }

    }

    return {

        onOffPanel: function () {
            toggle("id_panel_minuta_planteamiento", "blind",500);
            changeTheObDeniedWritingImg("id_img_minuta_planteamiento");
            initObjsPanel();
            document.getElementById("id_minuta_planteamiento").value=0;
        },

        valEnvio: function () {
            if (validateObjs(document.getElementById("id_panel_minuta_planteamiento")))
                rqsRegister();
        },

        edit: function (obj) {
            if(document.getElementById('id_panel_minuta_planteamiento').style.display == "none")
                MinutaPlanteamiento.onOffPanel();
            document.getElementById("id_minuta_planteamiento").value = obj.parentNode.parentNode.id;
            let oPanel = document.getElementById("id_panel_minuta_planteamiento");
            let oInputs = oPanel.getElementsByTagName("input");
            oInputs['ponente'].value = obj.parentNode.parentNode.cells[0].innerHTML;
            let oTextAreas=oPanel.getElementsByTagName("textarea");
            oTextAreas['planteamiento'].value = obj.parentNode.parentNode.cells[1].innerHTML;
            oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[2].innerHTML;
        },

        rqsRemove: function (id) {
            confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
                if (ok) {
                    let responseAjax = (response) => {
                        (response != 'B') ? msjAdmin(response) : reqGet();
                    }
                    let ajax = new sendAjax();
                    ajax.method = 'POST';
                    ajax.url = MYPATH + "remove";
                    ajax.data = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
                    ajax.funResponse = responseAjax;
                    ajax.b64 = true;
                    ajax.send();
                }
            });
        }

    };

})();

