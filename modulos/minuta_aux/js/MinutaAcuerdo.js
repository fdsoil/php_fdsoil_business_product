var MinutaAcuerdo = (function () {

    const MYPATH = PATH_MYAPP1 + "reqs/control/minuta/minuta_acuerdo.php/";

    let initObjsPanel = function() {
        initObjs(document.getElementById('id_panel_minuta_acuerdo'));
    }

    let rqsRegister = function() {
        let responseAjax = (response) => {
            msjAdmin(response);
            if (inArray(response, ['C', 'A'])){
                reqGet();
                MinutaAcuerdo.onOffPanel();
            }
        }
        let sIdVal = document.getElementById('id').value;
        let oPanel = document.getElementById('id_panel_minuta_acuerdo');
        let reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.b64 = true;
        ajax.send();
    }

    let reqGet = function (bAsync) {
        let responseAjax = (response) => {
            fillTab(response);
            valBtnClose();
        }
        let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "get";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'json';
        ajax.async = bAsync;
        ajax.b64 = true;
        ajax.send();
    }

    let fillTab = function (aObjs) {

        let fillTabAux = function(row) {

            let oTr = document.createElement('tr');
            oTr.id = row.id;
            let aTd = [];

            aTd[0] = document.createElement('td');
            aTd[0].appendChild(document.createTextNode(row.responsable));
            oTr.appendChild(aTd[0]);

            aTd[1] = document.createElement('td');
            aTd[1].appendChild(document.createTextNode(row.acuerdo));
            oTr.appendChild(aTd[1]);

            aTd[2] = document.createElement('td');
            aTd[2].appendChild(document.createTextNode(row.observacion));
            oTr.appendChild(aTd[2]);

            let oImgEdit = document.createElement('img');
            oImgEdit.setAttribute('class', 'accion');
            oImgEdit.setAttribute('src', PATH_APPORG +'img/edit.png');
            oImgEdit.setAttribute('title', 'Editar datos...');
            oImgEdit.setAttribute('onclick',"MinutaAcuerdo.edit(this);valBtnClose();");
            aTd[3] = document.createElement('td');
            aTd[3].align = 'center';
            aTd[3].appendChild(oImgEdit);

            let oImgDelete = document.createElement('img');
            oImgDelete.setAttribute('class', 'accion');
            oImgDelete.setAttribute('src', PATH_APPORG + 'img/cross.png');
            oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
            oImgDelete.setAttribute('onclick',"MinutaAcuerdo.rqsRemove("+row.id+");valBtnClose();");
            aTd[3].appendChild(oImgDelete);
            oTr.appendChild(aTd[3]);

            return oTr;

        }

        let tbl = document.getElementById('id_tab_minuta_acuerdo');
        Table.deleteAllRows('id_tab_minuta_acuerdo',1);

        if (!tbl.tBodies[0]) {
            var tbody = document.createElement('tbody');
            tbody.setAttribute('style', 'display: block');
            tbody.setAttribute('style', 'width: 100%');
            tbl.appendChild(tbody);
        }

        if (aObjs.length !== 0) {
            for (var i = 0; i < aObjs.length; i++)
                tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
            Table.paintTRsClearDark('id_tab_minuta_acuerdo');
        }

    }

    return {

        onOffPanel: function () {
            toggle("id_panel_minuta_acuerdo", "blind",500);
            changeTheObDeniedWritingImg("id_img_minuta_acuerdo");
            initObjsPanel();
            document.getElementById("id_minuta_acuerdo").value=0;
        },

        valEnvio: function () {
            if (validateObjs(document.getElementById("id_panel_minuta_acuerdo")))
                rqsRegister();
        },

        edit: function (obj) {
            if(document.getElementById('id_panel_minuta_acuerdo').style.display == "none")
                MinutaAcuerdo.onOffPanel();
            document.getElementById("id_minuta_acuerdo").value = obj.parentNode.parentNode.id;
            let oPanel = document.getElementById("id_panel_minuta_acuerdo");
            let oInputs = oPanel.getElementsByTagName("input");
            oInputs['responsable'].value = obj.parentNode.parentNode.cells[0].innerHTML;
            let oTextAreas=oPanel.getElementsByTagName("textarea");
            oTextAreas['acuerdo'].value = obj.parentNode.parentNode.cells[1].innerHTML;
            oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[2].innerHTML;
        },

        rqsRemove: function (id) {
            confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
                if (ok) {
                    let responseAjax = (response) => {
                        (response != 'B') ? msjAdmin(response) : reqGet();
                    }
                    let ajax = new sendAjax();
                    ajax.method = 'POST';
                    ajax.url = MYPATH + "remove";
                    ajax.data = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
                    ajax.funResponse = responseAjax;
                    ajax.b64 = true;
                    ajax.send();
                }
            });
        }

    };

})();

