function sendEnteAuxRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_ente') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_ente_aux'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/admin_ente/ente_aux_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendEnteAuxGet();
            onOffPanelEnteAux();
        }
    }
}

function sendEnteAuxGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/admin_ente/ente_aux_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabEnteAux(response);
        valBtnClose();
    }
}

function sendEnteAuxDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/admin_ente/ente_aux_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendEnteAuxGet();
    }
}

