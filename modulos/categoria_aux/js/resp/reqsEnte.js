function sendEnteRegister(sStatus)
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('div0'), true);// + '&id_status=' + sStatus;

    send_ajax('POST', "../../../"+myApp+"/reqs/admin_ente/ente_register.php", responseAjax, reqs, null, true, false, true);

    function responseAjax(response)
    {
        msjAdmin(response[0]);
        if (inArray(response[0], ['C', 'A', 'H'])) {
            if (response[0]=='C') {
                document.getElementById('id').value=response[1];
                show('Tab1', 'explode', 1500);
                //sendIdCodDateGet();
                eventListenerActivar();
            } else if (response[0]=='H') {
                document.getElementById('popup_ok').setAttribute('onClick', 'window.location.assign("../ente/")');
            }
        }
    }
}

function sendEnteGet(id)
{
    send_ajax('POST', "../../../"+myApp+"/reqs/admin_ente/ente_get.php", responseAjax, "id="+id, null, true, false);

    function responseAjax(response) 
    {
        document.getElementById('id').value = response.id;
        document.getElementById('descripcion').value = response.descripcion;
        if (response.id!=0) {
            sendEnteAuxGet(false);
        }
    }
}

//function sendIdCodDateGet()
//{
//    send_ajax( 'POST', "../../../"+myApp+"/reqs/ente/id_cod_date_get.php", responseAjax, null, null, true, false);
//
//    function responseAjax(response)
//    {
//        document.getElementById('id').value=response[0].id;
//        document.getElementById('id_cod').innerHTML=response[0].codigo;
//        document.getElementById('id_date').innerHTML=response[0].fecha_registro;
//    }
//}
//
