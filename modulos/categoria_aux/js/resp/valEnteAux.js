function onOffPanelEnteAux() 
{
    toggle('id_panel_ente_aux', 'blind',500);
    changeTheObDeniedWritingImg('id_img_ente_aux');
    initObjs(document.getElementById('id_panel_ente_aux'));
    document.getElementById('id_ente_aux').value=0;
}

function valPanelEnteAux()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_ente_aux');
 //   if ( validateObjs(document.getElementById('id_panel_ente_aux'))) {
    if (document.getElementById('descripcion_aux').value!='') {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioEnteAux()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioEnteAux()
{
    var oDiv=document.getElementById('id_panel_ente_aux');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendEnteAuxRegister(reqs);
}

function editEnteAux(obj)
{
    if(document.getElementById('id_panel_ente_aux').style.display=='none'){
        onOffPanelEnteAux();
    }
    document.getElementById('id_ente_aux').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_ente_aux");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['descripcion_aux'].value = obj.parentNode.parentNode.cells[0].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelEnteAux();
}

