function sendNivelGet(obj)
{

    document.getElementById("padre_id").value=obj.value;

    var nTr=document.getElementById("id_table_padre").getElementsByTagName('tr').length;
    var oTr=document.createElement('tr');

    var oTd1=document.createElement('td');
    oTd1.setAttribute("width","25%");

    oTd1.textContent='Nivel ('+nTr+')';
    oTr.appendChild(oTd1);

    var oTd2=document.createElement('td');
    oTd2.textContent=obj.options[obj.selectedIndex].text;
    oTd2.id=obj.value;
    oTd2.setAttribute("label" , obj.options[obj.selectedIndex].label);
    oTd2.setAttribute("onClick","sendNivelBackGet()");

    oTr.appendChild(oTd2);

    document.getElementById("id_table_padre").appendChild(oTr);

    var oTtdSeleccion=document.getElementById("id_td_seleccion");
    oTtdSeleccion.textContent='Selección (nivel '+(nTr+1)+')';

    var oTtdDescripcion=document.getElementById("id_td_descripcion");
    oTtdDescripcion.textContent='Descripción (nivel '+(nTr+1)+')';

    //var oTtdTipoEnte=document.getElementById("id_td_tipo_ente");
    //oTtdTipoEnte.textContent='Tipo de Ente (nivel '+(nTr+1)+')';


    var responseAjax = ( response ) =>
    {
        fillCombo(obj,response,'0','Seleccione...');
        //valBtnClose();
    }
    var ajax = new sendAjax();
    ajax.data =  "id=" + obj.value;
    ajax.url = "../../../" + aMyApp[1] + "/reqs/control/categoria/index.php/getNivel";
    ajax.funResponse = responseAjax;
    ajax.dataType='json';
    ajax.method='POST';
    ajax.send();
}

function sendNivelBackGet()
{
    var oTr=document.getElementById("id_table_padre").getElementsByTagName('tr');

    var id=oTr[oTr.length-1].cells[1].getAttribute("label");
    document.getElementById("id_table_padre").removeChild(oTr[oTr.length-1]);

    var oTtdSeleccion=document.getElementById("id_td_seleccion");
    oTtdSeleccion.textContent='Selección (nivel '+(oTr.length)+')';

    var oTtdDescripcion=document.getElementById("id_td_descripcion");
    oTtdDescripcion.textContent='Descripción (nivel '+(oTr.length)+')';

    //var oTtdTipoEnte=document.getElementById("id_td_tipo_ente");
    //oTtdTipoEnte.textContent='Tipo de Ente (nivel '+(oTr.length)+')';


    var responseAjax = ( response ) =>
    {
        fillCombo(document.getElementById("id_combo"),response,'0','Seleccione...');
        //valBtnClose();
    }
    var ajax = new sendAjax();
    ajax.data =  "id=" + id;
    ajax.url = "../../../"+ aMyApp[1] + "/reqs/control/categoria/index.php/getNivel";
    ajax.funResponse = responseAjax;
    ajax.dataType='json';
    ajax.method='POST';
    ajax.send();
}




