<?php
use \FDSoil\Func;
use \FDSoil\DbFunc;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = array_key_exists('id', $_POST)?$_POST['id']:0;
        $aRegist=array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(\myApp1\Categoria::get('REGIST'))
                : DbFunc::iniRegist('categoria','public', 'producto');
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);

        if (!$_POST) {
            $xtpl->assign('ID', $aRegist['id']);
            $xtpl->assign('ID_AUX', $aRegist['padre_id']);
            $xtpl->assign('NIVEL_DES', 0);
            $xtpl->assign('DISPLAY', "");
            $matrix = DbFunc::resultToArray(\myApp1\Categoria::get('NIVEL', 0));            
            foreach ($matrix as $arr) {
                $xtpl->assign('ID_COMBO', $arr['id']);
                $xtpl->assign('DES_COMBO', $arr['familia']);
                $xtpl->assign('LABEL_COMBO', $arr['padre_id']);
                $xtpl->parse('main.combo');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        } else {
            //Func::seeArray($aRegist);
            $xtpl->assign('ID', $aRegist['id']);
            $xtpl->assign('ID_AUX', $aRegist['padre_id']);
            //$aEnteDes = explode(" / ", \myApp1\Categoria::categoriaDesTipo($aRegist['id']));

           $aEnteDes = explode(" / ",$aRegist['familia']);

            $xtpl->assign('NIVEL_DES', count($aEnteDes)-1);
            $xtpl->assign('DISPLAY', "none");
            $xtpl->assign('DESCRIPCION', $aRegist['nombre']);
            //$xtpl->assign('ALIAS', $aRegist['alias']);
            for ( $i=0; $i < count($aEnteDes)-1; $i++ ) {
                $xtpl->assign('TR_NOM', 'Nivel ('.$i.')');
                $xtpl->assign('TR_DES', $aEnteDes[$i]);
                $xtpl->parse('main.tr');
            }             
            //$obj->seeArray($aEnteDes);
        }
        //$result = \myApp\AdminiEnte::adminiEnteTipoList();
        //while ($row = DbFunc::fetchRow($result)) {
        //    $xtpl->assign('ID_ENTE_TIPO', $row[0]);
        //    $xtpl->assign('DES_ENTE_TIPO', $row[1]);
        //    $xtpl->assign('SELECTED_ENTE_TIPO', ($aRegist['id_ente_tipo'] == $row[0]) ? 'selected' : '');
        //    $xtpl->parse('main.ente_tipo');
        //}
        Func::btnsPutPanel( $xtpl, 
            [
                ["btnName" => "Return", "btnBack" => "categoria"],
                ["btnName" => "Save", "btnClick"=> "valEnvio();"]
            ]
        );
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }    
}
