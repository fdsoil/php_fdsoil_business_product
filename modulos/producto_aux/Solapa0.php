<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;
use \myApp1\Producto;

trait Solapa0
{
    private function _solapa0()
    {
        $aRegist = array_key_exists('id', $_POST) ?
            DbFunc::fetchAssoc(Producto::get('REGIST')) :
                DbFunc::iniRegist('producto','public', 'producto');
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa0.html");
        Func::appShowId($xtpl);
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        $xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $result = Producto::categoriaList();
        while ($row = DbFunc::fetchAssoc($result)) {
            $xtpl->assign('ID_CATEGORIA', $row['id']);
            $xtpl->assign('DES_CATEGORIA', $row['familia']);
            $xtpl->assign('SELECTED_CATEGORIA', ($aRegist['id_categoria'] == $row['id']) ? 'selected' : '');
            $xtpl->parse('main.categoria');
        }
        $result = Producto::marcaList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_MARCA', $row[0]);
            $xtpl->assign('DES_MARCA', $row[1]);
            $xtpl->assign('SELECTED_MARCA', ($aRegist['id_marca'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.marca');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

