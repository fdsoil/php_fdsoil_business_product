<?php

trait ModalWindowEmpaque
{
    private function _modalWindowEmpaque()
    {
        $xtpl = new XTemplate(__DIR__."/modalWindowEmpaque.html");
        \FDSoil\Func::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

