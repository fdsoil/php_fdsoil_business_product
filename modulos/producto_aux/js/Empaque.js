let Empaque = (function () {
    let oLabelCantidad = document.getElementById("label_cantidad");
    let oIdPreCantidad = document.getElementById("id_pre_cantidad");
    let oIdPreEmpaque = document.getElementById("id_pre_empaque");
    let oPreEmpaque = document.getElementById("pre_empaque");
    let oMedidaUnidad = document.getElementById("medida_unidad");
    let oIdTdPreEmpaque = document.getElementById("id_td_pre_empaque");
    let oIdPreEmpaqueJson = document.getElementById("id_pre_empaque_json");
    let udm = oMedidaUnidad.value;
    let n = 0, i = 0, aJson = [];
    let _add = function () {
        let cleanAfter  = function () {
            oLabelCantidad.textContent = `Cantidad de ${empa}`;
            oIdPreCantidad.value = "";
            oIdPreEmpaque.value = "";
            oPreEmpaque.value = "";
        }
        let aConect = [ ' DE ', ' CON ' ]; 
        let empa = oPreEmpaque.value;
        let cant = oIdPreCantidad.value;
        let concatena1 = `${empa} ${aConect[n++]} ${cant} `;
        let concatena2 = ((oIdTdPreEmpaque.textContent.trim() == "") ? oMedidaUnidad.value : oIdTdPreEmpaque.textContent);
        oIdTdPreEmpaque.textContent = concatena1 + concatena2;
        if (n == 2)
          n = 0;
        aJson[i++] = `{"packing":"${empa}","quantity":${cant}}`;
        oIdPreEmpaqueJson.value=`[${aJson}]`;
        cleanAfter();
    }
    let _accept = function () {
        let despliegue = oIdTdPreEmpaque.textContent;
        document.getElementById("empaque").value = oIdPreEmpaqueJson.value;
        document.getElementById("empaque_desplegado").value = despliegue;
        Empaque.ModWin.hide("empaque");
    }
    let _remove = function () {
        oLabelCantidad.textContent = `Cantidad de ${udm}`;
        oIdPreCantidad.value = "";
        oIdPreEmpaque.value = "";
        oPreEmpaque.value = "";
        oIdTdPreEmpaque.textContent = "";
        oIdPreEmpaqueJson.value = "";
    }
    return {
        add   : () =>    _add(),
        accept: () => _accept(),
        remove: () => _remove(),
        ModWin: (function () {
            let _init = function () {
                document.getElementById("label_cantidad").textContent = "";
                document.getElementById("id_pre_cantidad").value = "";
                document.getElementById("id_pre_empaque").value = "";
                document.getElementById("pre_empaque").value = "";
                document.getElementById("id_td_pre_empaque").textContent = "";
                document.getElementById("id_pre_empaque_json").value = "";
                removeErrorMsg(document.getElementById('empaque_desplegado'));
            }
            let eventListener = function(){            
                let funCallBack = document.getElementById('medida_unidad').value 
                    ? Empaque.ModWin.show('empaque')
                        : displayErrorMsg(document.getElementById('empaque_desplegado'), 'Disculpe: Seleccione antes Unidad de Medida.'); 
                document.getElementById("empaque_desplegado").addEventListener( "click", function(){ return funCallBack; } );
            }            
            document.getElementById("empaque_desplegado").addEventListener( "focus", eventListener );
            return{
                show: function (strName) {
                    (function () {
                        _init();
                        let uMed = document.getElementById("medida_unidad").value;
                        let oCan = document.getElementById("label_cantidad");
                        oCan.textContent = 'Cantidad de ' + uMed;
                    })();
                    show("div_deshacer_fondo_opaco",'clip',250);
                    show("div_deshacer_modal_"+ strName,'clip',500);
                },
                hide: function (strName) {
                    (function () {
                        _init();
                    })(); 
                    hide("div_deshacer_fondo_opaco",'clip',250);
                    hide("div_deshacer_modal_" + strName,'clip',500);
                }
            }
        })()
    }
})();


