var Presentacion = (function () {

    const MYPATH = PATH_MYAPP + "reqs/control/producto/presentacion.php/";

    let initObjsPanel = function() {
        initObjs(document.getElementById('id_panel_presentacion'));
    }

    let rqsRegister = function() {
        let responseAjax = (response) => {
            msjAdmin(response);
            if (inArray(response, ['C', 'A'])){
                reqGet();
                Presentacion.onOffPanel();
            }
        }
        let sIdVal = document.getElementById('id').value;
        let oPanel = document.getElementById('id_panel_presentacion');
        let reqs = b64EncodeUnicode('id_producto') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.b64 = true;
        ajax.send();
    }

    let reqGet = function (bAsync) {
        let responseAjax = (response) => {
            fillTab(response);
            valBtnClose();
        }
        let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "get";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'json';
        ajax.async = bAsync;
        ajax.b64 = true;
        ajax.send();
    }

    let fillTab = function (aObjs) {

        let fillTabAux = function(row) {

            let oTr = document.createElement('tr');
            oTr.id = row.id;
            let aTd = [];

            aTd[0] = document.createElement('td');
            aTd[0].appendChild(document.createTextNode(row.medida_unidad));
            oTr.appendChild(aTd[0]);

            aTd[1] = document.createElement('td');
            aTd[1].id = row.empaque_json;
            aTd[1].appendChild(document.createTextNode(row.empaque_desplegado));
            oTr.appendChild(aTd[1]);

            aTd[2] = document.createElement('td');
            aTd[2].appendChild(document.createTextNode(row.precio_costo));
            oTr.appendChild(aTd[2]);

            aTd[3] = document.createElement('td');
            aTd[3].appendChild(document.createTextNode(row.precio_venta));
            oTr.appendChild(aTd[3]);

            aTd[4] = document.createElement('td');
            aTd[4].appendChild(document.createTextNode(row.bar_cod));
            oTr.appendChild(aTd[4]);

            aTd[5] = document.createElement('td');
            aTd[5].appendChild(document.createTextNode(row.int_cod));
            oTr.appendChild(aTd[5]);

            let oImgEdit = document.createElement('img');
            oImgEdit.setAttribute('class', 'accion');
            oImgEdit.setAttribute('src', PATH_APPORG +'img/edit.png');
            oImgEdit.setAttribute('title', 'Editar datos...');
            oImgEdit.setAttribute('onclick',"Presentacion.edit(this);valBtnClose();");
            aTd[6] = document.createElement('td');
            aTd[6].align = 'center';
            aTd[6].appendChild(oImgEdit);

            let oImgDelete = document.createElement('img');
            oImgDelete.setAttribute('class', 'accion');
            oImgDelete.setAttribute('src', PATH_APPORG + 'img/cross.png');
            oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
            oImgDelete.setAttribute('onclick',"Presentacion.rqsRemove("+row.id+");valBtnClose();");
            aTd[6].appendChild(oImgDelete);
            oTr.appendChild(aTd[6]);

            return oTr;

        }

        let tbl = document.getElementById('id_tab_presentacion');
        Table.deleteAllRows('id_tab_presentacion',1);

        if (!tbl.tBodies[0]) {
            var tbody = document.createElement('tbody');
            tbody.setAttribute('style', 'display: block');
            tbody.setAttribute('style', 'width: 100%');
            tbl.appendChild(tbody);
        }

        if (aObjs.length !== 0) {
            for (var i = 0; i < aObjs.length; i++)
                tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
            Table.paintTRsClearDark('id_tab_presentacion');
        }

    }

    return {
        onOffPanel: function () {
            toggle("id_panel_presentacion", "blind",500);
            changeTheObDeniedWritingImg("id_img_presentacion");
            initObjsPanel();
            document.getElementById("id_presentacion").value=0;
        },
        valEnvio: function () {
            if (validateObjs(document.getElementById("id_panel_presentacion")))
                rqsRegister();
        },
        edit: function (obj) {
            if(document.getElementById('id_panel_presentacion').style.display == "none")
                Presentacion.onOffPanel();
            document.getElementById("id_presentacion").value = obj.parentNode.parentNode.id;
            let oPanel = document.getElementById("id_panel_presentacion");
            let oInputs = oPanel.getElementsByTagName("input");
            let oTextarea = oPanel.getElementsByTagName("textarea");
            oInputs['medida_unidad'].value = obj.parentNode.parentNode.cells[0].textContent;
            oInputs['empaque'].value = obj.parentNode.parentNode.cells[1].id;
            oTextarea['empaque_desplegado'].value = obj.parentNode.parentNode.cells[1].textContent;
            oInputs['precio_costo'].value = obj.parentNode.parentNode.cells[2].textContent;
            oInputs['precio_venta'].value = obj.parentNode.parentNode.cells[3].textContent;
            oInputs['bar_cod'].value = obj.parentNode.parentNode.cells[4].textContent;
            oInputs['int_cod'].value = obj.parentNode.parentNode.cells[5].textContent;            
        },
        rqsRemove: function (id) {
            confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
                if (ok) {
                    let responseAjax = (response) => {
                        (response != 'B') ? msjAdmin(response) : reqGet();
                    }
                    let ajax = new sendAjax();
                    ajax.method = 'POST';
                    ajax.url = MYPATH + "remove";
                    ajax.data = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
                    ajax.funResponse = responseAjax;
                    ajax.b64 = true;
                    ajax.send();
                }
            });
        },
        pickOpen: function () {
               pickOpen('medida_unidad',
                     'id_medida_unidad',
                     '../../../' + appOrg + '/pickList/control/unidad_de_medida/', 35, 75, 200, 50, 'si');
        }
    };

})();



