<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp1\Producto\Presentacion;

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa1.html");
        Func::appShowId($xtpl);
        if (array_key_exists('id', $_POST)) {
            $matrix = Presentacion::get();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('MEDIDA_UNIDAD', $arr['medida_unidad']);
                $xtpl->assign('EMPAQUE_DESPLEGADO', $arr['empaque_desplegado']);
                $xtpl->assign('EMPAQUE_JSON', $arr['empaque_json']);
                $xtpl->assign('PRECIO_COSTO', $arr['precio_costo']);
                $xtpl->assign('PRECIO_VENTA', $arr['precio_venta']);
                $xtpl->assign('BAR_COD', $arr['bar_cod']);
                $xtpl->assign('INT_COD', $arr['int_cod']);                
                $xtpl->parse('main.tab_presentacion');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

