<?php
use \myApp1\Producto\Presentacion;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register() { echo base64_encode(Presentacion::register()); }

    private function get() { echo base64_encode(json_encode(Presentacion::get())); }

    private function remove() { echo base64_encode(Presentacion::remove()); }

}

