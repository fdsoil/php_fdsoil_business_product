<?php
use \myApp1\Minuta\MinutaAsistente;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register() { echo base64_encode(MinutaAsistente::register()); }

    private function get() { echo base64_encode(json_encode(MinutaAsistente::get())); }

    private function remove() { echo base64_encode(MinutaAsistente::remove()); }

}

