<?php
use \myApp1\Minuta\MinutaAcuerdo;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register() { echo base64_encode(MinutaAcuerdo::register()); }

    private function get() { echo base64_encode(json_encode(MinutaAcuerdo::get())); }

    private function remove() { echo base64_encode(MinutaAcuerdo::remove()); }

}

