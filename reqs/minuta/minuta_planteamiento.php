<?php
use \myApp1\Minuta\MinutaPlanteamiento;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register() { echo base64_encode(MinutaPlanteamiento::register()); }

    private function get() { echo base64_encode(json_encode(MinutaPlanteamiento::get())); }

    private function remove() { echo base64_encode(MinutaPlanteamiento::remove()); }

}

